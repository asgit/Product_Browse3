<%--
  Created by IntelliJ IDEA.
  User: user
  Date: 2017/11/23
  Time: 19:50
  To change this template use File | Settings | File Templates.
--%>
<%@ page import="cn.wzr.global.Const" %>
<%@ page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE HTML>
<html>
  <head>
    <title>商品图片批量上传</title>
    <%
        String multiUploadScript = request.getContextPath() + Const.WEBURL_SEPARATOR
                + Const.SCRIPT_FOLDER_NAME + Const.WEBURL_SEPARATOR + Const.CMDT_FOLDER_NAME + "photo_multi_upload.js";
        String multiUploadCss = request.getContextPath() + Const.WEBURL_SEPARATOR
                + Const.STYLE_FOLDER_NAME + "multiUpload.css";
        String actionUrlMultiUpload = request.getContextPath() + Const.WEBURL_SEPARATOR
                + Const.ACT_MULTIMG_UPLOAD;
        String actionUrlMuBarCodeValidate = request.getContextPath() + Const.WEBURL_SEPARATOR
                + Const.ACT_MU_BARCODE_VALIDATE;
    %>
    <script src="<%=multiUploadScript%>"></script>

    <link rel="stylesheet" type="text/css" href="<%=multiUploadCss%>">

  </head>
<body>

    <input type="hidden" id="actionUrlMultiUpload" value="<%= actionUrlMultiUpload%>" />
    <input type="hidden" id="actionUrlMuBarCodeValidate" value="<%= actionUrlMuBarCodeValidate%>" />

    <form id="uploadForm" method="POST">
        <%--enctype="multipart/form-data"--%>
        <div>
            每件商品图片数:
            <input type="number" id="imgCountPerCmdt" class="numberBox" value="4" min="1" max="10"
                   title="取值范围：1-10">
        &nbsp;&nbsp;&nbsp;&nbsp;
            生产卡所在位置:
            <input type="number" id="indexOfProductCard" class="numberBox" value="4" min="0" max="10"
                title="取值范围：0-10(0表示无生产卡图片)">
        </div>

        <%--
        <div>
            <label title="只接收文本文件, 每行保存一个条码信息" >
                商品条码所在文件:
            </label>
            <input type="file" id="barCodesFile" accept=".txt"></br>
        </div>
        --%>
        <div>
            <label id="labelUploadImgs" title="选择文件数量必须为'每件商品图片数'的整数倍"
                   for="uploadFiles">
                商品条码及图片文件
            </label>
            <input type="file" id="uploadFiles" accept=".jpg, .jpeg, .png, .txt" multiple onchange="uploadFilesChanged()">
            <button type="reset">重置</button>
            <button id="btnUpload" type="button" onclick="submitClick()">补充图片</button>
            <p id="barCodeTxtInfo"></p>
            <p id="actionResult"></p>
        </div>
        <div class="preview">
            <p>No files currently selected for upload</p>
        </div>
    </form>

</body>
</html>
