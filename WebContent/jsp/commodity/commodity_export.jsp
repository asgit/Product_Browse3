<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<title>导出商品信息</title>
<%
  request.setAttribute("baseURL", request.getContextPath());
%>
  <script type="text/javascript">  
    var baseURL = "${baseURL}";
  </script> 
  <script src="${baseURL}/script/commodity/commodity_export.js"></script>
</head>
<body onload="bodyLoad()">
  <form action= "${baseURL}/CmdtsExport">
    <input type="submit" value="导出..."/>
  </form>
  <table>
    <tr>
      <td><img alt="p" src="${baseURL}/image/1.jpg" width="100">
    </tr>
  </table>
  <button type="button" onClick="return exportClick()">导出数据...</button>
</body>
</html>