/**
 * 商品批量上传
 */

/**
 * [商品信息文件]的对象
 */
var commodityTxtFile;

/**
 * 用户选择的图片文件对象
 */
var imageFiles;

/**
 * [商品信息]的校验结果,是一个JSON对象,其中包含了上传路径
 */
var cmdtsValidateResult;

/**
 * [商品文件]内容数组
 */
var cmdtsTxtArray;

/**
 * 页面上用于上传图片按钮的标签ID
 * @type {string}
 */
const INP_UPLOAD_IMG_ID = "uploadFiles";

/**
 * 提交时用到的图片对象名字, 服务端取数据会用到
 * @type {string}
 */
const VARN_IMAGEFILES = "imageFiles";

window.onload = function pageload() {
    /*var inpUploadTxt = document.getElementById('barCodesFile');
    inpUploadTxt.addEventListener("change",accessFiles);
*/
    var inpUploadImgs = document.getElementById(INP_UPLOAD_IMG_ID);
    inpUploadImgs.style.opacity = 0;

    /*inpUploadImgs.addEventListener("change", function () {
        self.uploadFilesChanged();
    });*/
}

/**
 * 用户选择了[商品文件]和图片所触发的事件
 * 读取[商品文件], ajax提交到后台,
 * 按条码查询数据库, 生成json数据{商品对象{条码,款号,颜色,长度,尺码,展号,图片名},异常描述}.
 * 图片名为上传后保存到服务器上用的全路径名:命名规则为:款号\条码-#.jpg,#代表序号,多图片间用分号间隔
 * 异常包括: (查到商品)图片已存在.
 * 异常描述为空代表正常.实际提交时只上传正常的图片
 * 查询时要显示: 正在核对商品信息,请稍候...
 */
function uploadFilesChanged() {
	cmdtsTxtArray = null;

	// 获取文件列表对象
    var inpUploadImgs = document.getElementById(INP_UPLOAD_IMG_ID);
    var files = inpUploadImgs.files;
    var imgFiles = [];
    var txtFile;

    var j = 0;
    for (var i = 0; i < files.length; ++i) {
        if (validImageType(files[i])) {
            imgFiles[j++] = (files[i]);
        } else {
            txtFile = files[i];
        }
    }
    commodityTxtFile = txtFile;
    imageFiles = imgFiles;  // set image files object
    cmdtAttachToImgs(imgFiles);
}


/**
 * 商品图片数,生产卡所在位置, [商品条码文件]的有效行数, 等数量的校验
 * 成功返回空串, 失败返回问题描述
 */
function imgsCountValidate() {
    // 每件商品图片数
    var imgCountPerCmdt = document.getElementById("imgCountPerCmdt").value;
    // 生产卡所在位置
    var indexOfProductCard = document.getElementById("indexOfProductCard").value;

    var errInfo;
    if (imgCountPerCmdt < indexOfProductCard) {
        errInfo = "[生产卡所在位置]超出[每件商品图片数]";
        return errInfo;
    }

    if (null == cmdtsTxtArray || 0 == cmdtsTxtArray.length) {
        errInfo = "没有选择有效的[商品条码文件]";
        return errInfo;
    }

}

/**
 *  按照设定好的[每件商品图片数]等参数读取选定的图片, 并生成预览
 *  若参数错误(出现矛盾)则停止预览, 显示错误信息
 */
function updateImageDisplay(selectedImgFiles, errInfo) {
    var preview = document.querySelector('.preview');
    while (preview.firstChild) {
        preview.removeChild(preview.firstChild);
    }

    if (undefined != errInfo && null != errInfo && "" !== errInfo) {
        var para = document.createElement('p');
        para.textContent = errInfo;
        preview.appendChild(para);
        return;
    }

    if (null == commodityTxtFile) {
        var para = document.createElement('p');
        para.textContent = "没有选择有效的[商品条码文件]";
        preview.appendChild(para);
        return;
    }

    // 每件商品图片数
    var imgCountPerCmdt = document.getElementById("imgCountPerCmdt").value;

    // var inpUploadImgs = document.getElementById(INP_UPLOAD_IMG_ID);

    var cmdtCount;  // 由图片数算出来的商品数
    // var selectedImgFiles = inpUploadImgs.files; // 选中的待上传图片对象
    if (!(selectedImgFiles.length % imgCountPerCmdt)) {
        // 文件数可以被 [每件商品图片数] 整除
        cmdtCount = selectedImgFiles.length / imgCountPerCmdt; // 计算商品数量
        if (cmdtCount !== cmdtsTxtArray.length) {
            var para = document.createElement('p');
            para.textContent = "商品条码文件中商品数量(" + cmdtsTxtArray.length
                + ")与选择的图片数(" + selectedImgFiles.length + ")不匹配";
            preview.appendChild(para);
            return;
        }
    }
    else {
        var para = document.createElement('p');
        para.textContent = '[每件商品图片数]与选择的上传文件数不符';
        preview.appendChild(para);
        return;
    }

    // 生产卡所在位置
    var indexOfProductCard = document.getElementById("indexOfProductCard").value;
    var para;
    if (selectedImgFiles.length === 0) {
        para = document.createElement('p');
        para.textContent = 'No files currently selected for upload';
        preview.appendChild(para);
    } else {
        var list = document.createElement('ol');
        preview.appendChild(list);
        for (var iCmdt = 0; iCmdt < cmdtCount; iCmdt++) {
            // 遍历商品
            var listItem = document.createElement('li');
            para = document.createElement('p');
            for (var iCmdtImg = 0; iCmdtImg < imgCountPerCmdt; ++iCmdtImg) {
                // 遍历每件商品中的图片
                var realIndex = iCmdt * imgCountPerCmdt + iCmdtImg; // 图片的实际索引
                if (validImageType(selectedImgFiles[realIndex])) {
                    /*if(0 !== indexOfProductCard && (indexOfProductCard - 1) === iCmdtImg){
                        // 如果图片中存在生产卡, 且当前图片为生产卡, 则跳过
                        continue;
                    }*/
                    var image = document.createElement('img');
                    image.src = window.URL.createObjectURL(selectedImgFiles[realIndex]);

                    listItem.appendChild(image);
                    if (iCmdtImg == imgCountPerCmdt - 1) {
                        // 最后加上条码
                        var validBarCode = cmdtsTxtArray[iCmdt]; //.replace(/^\s+/,"").replace(/s+$/,"");
                        if (null != validBarCode && "" !== validBarCode) {
                            var barCodeSpan = document.createElement("span");   // 每个条码存在一个span中
                            barCodeSpan.innerText = validBarCode;
                            barCodeSpan.class = "barCodeSpan";
                            listItem.appendChild(barCodeSpan);
                        }

                    }

                } else {
                    var para = document.createElement('p');
                    para.textContent = 'File name ' + selectedImgFiles[iCmdt].name + ': Not a valid file type. Update your selection.';
                    listItem.appendChild(para);
                }
            }

            list.appendChild(listItem);
        }
    }
}

/**
 * 将读取[商品文件]得到的条码数组转换为Json, 以ajax方式提交到后台
 * 按条码查询数据库, 生成json数据{商品信息,上传保存路径,异常描述}.
 * 路径规则为: 上传根/图片/年份/款号/ (例:upload/image/17219-80/)
 * 图片名为上传后保存到服务器上用的文件名:命名规则为:条码-Number.jpg
 * 异常包括:  (查到商品)图片已存在.
 * 异常描述为空代表正常.实际提交时只上传正常的图片
 * 校验商品信息结果返回给全局变量cmdtsValidateResult
 */
function uploadMultiCmdtValidate(cmdtsArray) {
    var cmdtJsonAry = JSON.stringify(cmdtsArray);
    var actionResult = document.getElementById("actionResult");
    actionResult.innerText = "正在核对商品信息,请稍候...";
    var xhr = new XMLHttpRequest();
    var url = document.getElementById("actionUrlMultiValidate").value;
    xhr.open("POST", url, true);
    var formdata = new FormData();
    formdata.append("cmdtsJsonAry", cmdtJsonAry);
    xhr.onreadystatechange = function () {
        if (4 === xhr.readyState) {
            if (200 === xhr.status) {
            	cmdtsValidateResult = eval("(" + xhr.responseText + ")");
            	var actionResult = document.getElementById("actionResult");
            	if(null != cmdtsValidateResult.errInfo){
            		actionResult.innerText = cmdtsValidateResult.errInfo;
            	}else{
            		actionResult.innerText = "条码文件校验完毕";
            	}

            }else if(500 === xhr.status){
                var actionResult = document.getElementById("actionResult");
                actionResult.innerText = "服务器说, 没有找到请求的资源";

            }
        }
    }
    xhr.send(formdata);
}


/**
 * 得到了[商品信息所在文件]后读取其记录(行)数
 * 然后继续读取图片信息
 * @param imgFiles : 用户选择的所有图片
 */
function cmdtAttachToImgs(imgFiles) {
    if (commodityTxtFile == null) {
        document.getElementById("cmdtTxtInfo").innerHTML = "没有选择条码文件.";
        // barCodeTxtRows = 0;
        cmdtsTxtArray = null;
        return;

    }
    if (1024 * 1024 < commodityTxtFile.size) {
        // 如果barCode文件大于1MB, 可能选错了文件(正常的商品信息文件不可能这么大)
        document.getElementById("cmdtTxtInfo").innerHTML = "条码文件不能大于1MB.";
        // barCodeTxtRows = 0;
        cmdtsTxtArray = null;
        return;
    }

    var reader = new FileReader();
    reader.onload = function (event) {
        // 当读到商品信息文件的内容后, 执行如下代码:
        // 按行读取到数组中
    	var cmdtsRawArray = event.target.result.split("\r\n");
         
        for(var i=0; i<cmdtsRawArray.length; i++){
        	if(cmdtsRawArray[i].trim().length > 0){
        		// 此行内容不为空则添加到cmdtsTxtArray中
        		if(null == cmdtsTxtArray){
        			cmdtsTxtArray = new Array();
        		}
        		cmdtsTxtArray.push(cmdtsRawArray[i].trim());
        	}
        }
        document.getElementById("cmdtTxtInfo").innerHTML = "商品信息文件合计有效行数: " + cmdtsTxtArray.length + " 行";

        // 将读取[商品信息文件]得到的商品信息数组, 以ajax方式提交到后台
        // 校验商品信息结果返回给全局变量cmdtsValidateResult
        uploadMultiCmdtValidate(cmdtsTxtArray);

        // 校验页面上关于图片数量的参数是否正确
        var errInfo = imgsCountValidate();
        // 根据参数预览图片
        updateImageDisplay(imgFiles, errInfo);
    };
    reader.readAsText(commodityTxtFile,"utf-8");
}

var fileTypes = [
    'image/jpeg',
    'image/pjpeg',
    'image/png'
]

function validImageType(file) {
    for (var i = 0; i < fileTypes.length; i++) {
        if (file.type === fileTypes[i]) {
            return true;
        }
    }
    return false;
}

/**
 * 解析服务器的校验结果, 将可上传的图片加入到Formdata对象中
 * @return formdata 可上传图片对象
 */
function fillValidImage(formdata) {
    // 每件商品图片数
    var imgCountPerCmdt = parseInt(document.getElementById("imgCountPerCmdt").value);
    // 生产卡所在位置
    var indexOfProductCard = parseInt(document.getElementById("indexOfProductCard").value);

    // ++++过滤掉无效的图片++++
    var ow = document.getElementById("overwrite").checked;
    var cmdtJsonAry = cmdtsValidateResult.cmdtJsonArray;  // 校验结果对象数组
    // 单个商品的校验结果
    var imgInfo;
    for (var i = 0; i < cmdtJsonAry.length; ++i) {
        imgInfo = cmdtJsonAry[i];
        if (ow == true || (false == ow && "" == imgInfo.EceptInfo)) {
            // 无异常,图片有效
            for (var ind = 0; ind < imgCountPerCmdt; ++ind) {
                // 生成上传后的文件名, 跳过生产卡图片
                if (indexOfProductCard - 1 === ind) {
                	// 如果当前索引指向生产卡图片,则跳过
                    continue;
                } else {
                	// 原图片文件名
                    var curName = imageFiles[i * imgCountPerCmdt + ind].name;
                    // 图片扩展名
                    var extension = getExtension(curName);
                    // 构造文件名, 后台依赖此文件名(通过截取文件名前端的条码)修改相应数据库记录
                    var imgFileName = imgInfo.cmdtObj.Barcode + "-" + (ind + 1) + extension;
                    // 3个参数分别为: 路径,文件对象,文件名
                    formdata.append(imgInfo.ImageFolder, imageFiles[i * imgCountPerCmdt + ind], imgFileName);
//                    console.log("编码:" + imgInfo.cmdtObj.Barcode + )
                }
            }
        }
    }
}

/**
 * 判断FormData对象是否为空
 * @param formdata
 * @return {boolean}
 */
function formDataIsEmpty(formdata) {
    var len = 0;
    for(var pair of formdata.entries()) {
        ++len;
        break;
    }
    return (0 == len);
}

function multiCmdtSubmit() {
    if (null == commodityTxtFile || null == imageFiles || null == cmdtsValidateResult) {
        // 没有选择有效的数据
        var actionResult = document.getElementById("actionResult");
        actionResult.innerText = "没有符合上传条件的图片,请重新选择";
        return;
    }

    var formdata = new FormData();
    fillValidImage(formdata);
    // noinspection JSAnnotator

    if (formDataIsEmpty(formdata)) {
        var actionResult = document.getElementById("actionResult");
        actionResult.innerText = "没有符合上传条件的图片,请重新选择";
        return;
    }
    // 每件商品图片数
    var imgCountPerCmdt = document.getElementById("imgCountPerCmdt").value;
    // 生产卡所在位置
    var indexOfProductCard = document.getElementById("indexOfProductCard").value;

    // 每商品上传的图片数(不含生产卡)
    var imgUploadCountPerCmdt = (indexOfProductCard == 0) ? imgCountPerCmdt : (imgCountPerCmdt - 1);
    formdata.append("imgUploadCountPerCmdt", imgUploadCountPerCmdt.toString());

//    formdata.append("cmdtJsonArray",JSON.stringify(cmdtsValidateResult.cmdtJsonArray));
    formdata.append("cmdtsTxtArray",cmdtsTxtArray.toString());
// formdata.append(VARN_IMAGEFILES, imageFiles);
    var xhr = new XMLHttpRequest();
    var url = document.getElementById("actionUrlMultiUpload").value;
    xhr.open("POST", url, true);
    var actionResult = document.getElementById("actionResult");
    actionResult.innerText = "正在上传...";
    xhr.onreadystatechange = function () {
        if (4 === xhr.readyState) {
            if (200 === xhr.status) {
                var resultJson = eval("(" + xhr.responseText + ")");
                var actionResult = document.getElementById("actionResult");
                if(null == resultJson.errInfo || "" == resultJson.errInfo){
                    actionResult.innerText = resultJson.resultInfo;
                }else {
                    actionResult.innerText = resultJson.errInfo;
                }

                cmdtsValidateResult = null;
                document.getElementById("uploadForm").reset();  // 重置表单, 防止重复提交
            }
        }
    }
    xhr.send(formdata);

}

/**
 * 取文件扩展名(包括点号)
 * @param file_name
 * @returns {string}
 */
function getExtension(file_name) {
    var result = file_name.substring(file_name.lastIndexOf('.'));
    return result;
}

//js获取项目根路径，如： http://localhost:8083/uimcardprj
function getRootPath() {
    //获取当前网址，如： http://localhost:8083/uimcardprj/share/meun.jsp
    var curWwwPath = window.document.location.href;
    //获取主机地址之后的目录，如： uimcardprj/share/meun.jsp
    var pathName = window.document.location.pathname;
    var pos = curWwwPath.indexOf(pathName);
    //获取主机地址，如： http://localhost:8083
    var localhostPaht = curWwwPath.substring(0, pos);
    //获取带"/"的项目名，如：/uimcardprj
    var projectName = pathName.substring(0, pathName.substr(1).indexOf('/') + 1);
    return (localhostPaht + projectName);
}

function returnFileSize(number) {
    if (number < 1024) {
        return number + 'bytes';
    } else if (number > 1024 && number < 1048576) {
        return (number / 1024).toFixed(1) + 'KB';
    } else if (number > 1048576) {
        return (number / 1048576).toFixed(1) + 'MB';
    }
}