package cn.wzr.controller;

import cn.wzr.dao.impl.CommodityDao;
import cn.wzr.entity.Commodity;
import cn.wzr.global.Const;
import cn.wzr.util.CommonTools;
import cn.wzr.util.upload.UploadImageStrategy;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet(Const.WEBURL_SEPARATOR + Const.ACT_MU_BARCODE_VALIDATE)
@MultipartConfig
public class MuBarCodeValidate extends HttpServlet {

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException {

        Object obj = JSONValue.parse(request.getParameter("barCodeJsonAry"));
        JSONArray bcJsonAry = (JSONArray)obj;

        String contextPath = getServletContext().getRealPath(String.valueOf(Const.WEBURL_SEPARATOR));
        // 检测:条码不存在(查不到商品) 或 (查到商品)图片已存在.
        // 获得数据库操作的DAO
        CommodityDao commodityDao = CommonTools.getCommodityDao(contextPath);
        JSONArray imgJsonAry = new JSONArray();
        for(Object bc : bcJsonAry){
            String bcStr = bc.toString().trim();
            JSONObject imgJson = new JSONObject();
            Commodity commodity = commodityDao.getByBarCode(bcStr);

            // 生成json数据{条码,上传保存路径,图片名,异常描述}.
            if(null == commodity){
                imgJson.put(Const.JSP_P_CMDT_BARCODE,bcStr);
                imgJson.put(Const.JSP_P_CMDT_IMAGEPATH,"");
                imgJson.put(Const.JSP_P_CMDT_EXCPINFO,"未在数据库中查到此条码");
            }else {
                if(commodity.imgFilesIsValid(contextPath)){
                    // 文件存在, 可读, 大小不为0, 扩展名不为空
                    imgJson.put(Const.JSP_P_CMDT_BARCODE,bcStr);
                    imgJson.put(Const.JSP_P_CMDT_IMAGEPATH,"");
                    imgJson.put(Const.JSP_P_CMDT_EXCPINFO,"图片已存在");
                }else {
                    // 路径不存在或路径指向了空文件, 则可以上传文件
                    // 路径规则为: 上传根/图片/年份/款号/ (例: /upload/image/2017/17219-80/)
                    // 图片名为上传后保存到服务器上用的文件名:命名规则为:条码-序号.jpg
                    // 保存图片的文件夹名,例:"17219-80/"
                    String imgUploadFolderName = commodity.getStyleNo();
                    UploadImageStrategy uploadImageStrategy = new UploadImageStrategy(contextPath);
                    // 上传的相对路径,例: upload/image/2017/17219-80/
                    String imgUploadRelativePath = uploadImageStrategy.getUploadRelativePath(imgUploadFolderName);
                    imgJson.put(Const.JSP_P_CMDT_BARCODE,bcStr);
                    imgJson.put(Const.JSP_P_CMDT_IMAGEPATH,imgUploadRelativePath);
                    imgJson.put(Const.JSP_P_CMDT_EXCPINFO,"");
                }
            }
            imgJsonAry.add(imgJson);
        }
        JSONObject resultJsonObject = new JSONObject();
        resultJsonObject.put("imageJsonAry",imgJsonAry);    // 校验后生成的结果数组
        PrintWriter out = response.getWriter();
        out.print(resultJsonObject);
    }


    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doPost(request,response);
    }
}
