package cn.wzr.entity;

import cn.wzr.util.CommonTools;

import java.io.File;

public class Commodity {

    private Long id;
    /**
     * 展号
     */
    private String showNo = "";

    /**
     * 条码
     */
    private String cmdtSN = "";

    private String styleNo = "";

    /**
	 * 用于数据库操作时使用反射获取当前类的方法
	 */
	public Commodity() {
		super();
	}

	/**
	 * @param cmdtLineStr
	 */
	public Commodity(String cmdtLineStr) throws ArrayIndexOutOfBoundsException{
		String[] cmdtStrs = cmdtLineStr.split("\t");
		this.setCmdtSN(cmdtStrs[0]);
		this.setStyleNo(cmdtStrs[1]);
		this.setName(cmdtStrs[2]);
		this.setShowNo(cmdtStrs[5]);
	}

	/**
     * 取商品图片相对(应用的)文件夹路径
     *
     * @return
     */
    public String getImageFolderPath() {
        return imageFolderPath;
    }

    public void setImageFolderPath(String imageFolderPath) {
        this.imageFolderPath = imageFolderPath;
    }

    /**
     * 商品图片相对(应用的)文件夹路径
     */
    private String imageFolderPath = "";

    private String name = "";

    private String imagePath = "";

    public String getStyleNo() {
        return styleNo;
    }

    public void setStyleNo(String styleNo) {
        this.styleNo = styleNo;
    }

    public String getImagePath() {
        return imagePath;
    }

    public void setImagePath(String imagePath) {
        this.imagePath = (null == imagePath) ? "" : imagePath;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    /**
     * 获取展号
     *
     * @return
     */
    public String getShowNo() {
        return showNo;
    }

    public void setShowNo(String showNo) {
        this.showNo = showNo;
    }

    /**
     * 获取编号
     *
     * @return
     */
    public String getCmdtSN() {
        return cmdtSN;
    }

    public void setCmdtSN(String cmdtSN) {
        this.cmdtSN = cmdtSN;
    }

    /**
     * 判断当前商品文件的图片是否存在, 可读, 大小不为0, 扩展名不为空
     * @param contextPath 应用的绝对路径
     * @return 图片合法返回true
     */
    public boolean imgFilesIsValid(String contextPath) {
        // 图片相对于应用的路径
        String imgRelativePath;
        String[] cmdtImgs = getImagePath().split(";");
        File file;
        // 取图片真实路径, 相对于应用的路径
        for (String cmdtImg : cmdtImgs) {
            if (null == getImageFolderPath() || getImageFolderPath().isEmpty()) {
                imgRelativePath = cmdtImg;
            } else {
                imgRelativePath = getImageFolderPath() + cmdtImg;
            }
            file = new File(contextPath + imgRelativePath);  // 取出第一个图片路径
            if (CommonTools.isReadableExtFile(file) && !imgRelativePath.isEmpty()) {
                return true;
            }
        }
        return false;
    }
}
