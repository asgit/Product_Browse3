package cn.wzr.util.upload;

import cn.wzr.global.Const;

/**
 * <p>经过校验后合法的图片, 提交过来生成图片(组)信息</p>
 * <p>此类用于解析前端页面发来的图片信息</p>
 * 此类中字段对应商品对象, 用于上传图片后更新数据库
 */
public class UploadValidImage {

    private final static String HEADER_NAME = "; name=\"";
    private final static String HEADER_FILENAME = "; filename=\"";

    private String cmdtSN;
    private String imageFolderPath;

    private String lastImageName;

    private String shortImageNames;

    public String getLongImageNames() {
        return longImageNames;
    }

    /**
     * 长格式图片名组合字符串, 多个图片名间以指定分隔符分隔
     * 长格式, 即带有相对路径的文件名
     */
    private String longImageNames;

    /**
     * 读取商品条码
     * @return
     */
    public String getCmdtSN() {
        return cmdtSN;
    }

    /**
     * 读取当前商品图片的上传文件夹(相对于应用的路径)
     * @return
     */
    public String getImageFolderPath() {
        return imageFolderPath;
    }

    /**
     * 读取最后一次添加的图片文件名
     *
     * @return
     */
    public String getLastImageName() {
        return lastImageName;
    }

    /**
     * 所有成员变量置空
     */
    public void reset(){
        this.cmdtSN = "";
        this.shortImageNames = "";
        this.longImageNames = "";
        this.imageFolderPath = "";
        this.lastImageName = "";
    }
    /**
     * 将前端页面上传的图片信息分割, 整理
     * 生成[上传路径], [文件名], [条码], 填充到属性中
     *
     * @param partContent Part对象的图片部分信息("头信息的content-disposition"部分)
     *                     (如:form-data; name="upload\image\17732-75\"; filename="B00900564-3.JPG")
     *                     正确得到条码的前提是, 文件名的命名规则为:条码-Number.jpg
     */
    public void appendImage(String partContent) {
        int nameIndex = partContent.indexOf(HEADER_NAME);
        int filenameIndex = partContent.indexOf(HEADER_FILENAME);
        if (0 < nameIndex) {
            // 如果含有; name="表示后面字符串为路径
            this.imageFolderPath = partContent.substring(nameIndex + HEADER_NAME.length(), filenameIndex - 1);
        }

        if (0 < filenameIndex) {
            // 如果含有; filename="表示后面字符串为文件名
            String imgName = partContent.substring(filenameIndex + HEADER_FILENAME.length(), partContent.length() - 1);
            this.shortImageNames = (null != this.shortImageNames && !this.shortImageNames.isEmpty()) ?
                    (this.shortImageNames + Const.IMAGE_SEPARATOR + imgName) : imgName;
            this.longImageNames = (null != this.longImageNames && !this.longImageNames.isEmpty()) ?
                    (this.longImageNames + Const.IMAGE_SEPARATOR + this.imageFolderPath + imgName) :
                    (this.imageFolderPath + imgName);
            this.lastImageName = imgName;   // 最后一次添加的图片文件名

            int barCodeEndIndex = imgName.indexOf(Const.FILE_SUBFIX_SEPARATOR);
            if (0 < barCodeEndIndex) {
                // 在文件名中取条码
                if (null == this.cmdtSN || this.cmdtSN.isEmpty()) {
                    this.cmdtSN = imgName.substring(0, barCodeEndIndex);
                }
            }
        }
    }
}
