package cn.wzr.util.upload;

import cn.wzr.global.Const;

import java.io.File;
import java.time.Year;

public class UploadImageStrategy implements UploadStrategy {

    public UploadImageStrategy(String contentPath) {
        this.contentPath = contentPath;
    }

    private String contentPath;

    /**
     * 取相对于应用程序的上传路径
     * @param entityFolderName 保存图片的文件夹名(如:17129-80)
     * @return 返回保存图片的相对于应用程序的上传路径(如: upload/image/2017/17129-80/)
     */
    @Override
    public String getUploadRelativePath(String entityFolderName) {
        UploadProperty uploadProperty = new UploadProperty(contentPath);
        // 上传[图片/年份]的根目录( upload/image/year)
        Year year = Year.now();
        String imgUploadRootPath = uploadProperty.getUploadRelativePath(Const.PATH_IMAGE + File.separator + year.toString());
        // 如: upload/image/2017/17129-80/
        return (null == entityFolderName || entityFolderName.isEmpty()) ? imgUploadRootPath : (imgUploadRootPath + entityFolderName + File.separator);
    }
}
