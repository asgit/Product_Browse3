package cn.wzr.util.upload;

import cn.wzr.global.Const;
import cn.wzr.util.PropertiesMan;

import java.io.File;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.regex.Pattern;

/**
 * 从[upload属性文件]中读取配置信息的类
 *
 */
public class UploadProperty {

    private PropertiesMan propertiesMan;

    private String encoding;

    public String getEncoding() {
        return encoding;
    }

    /**
     * 设置上传所用编码
     */
    private void setEncoding() {
        encoding = propertiesMan.getValue(Const.UPLOAD_ENCODING);
        if(encoding.isEmpty()){
            encoding = Const.ENCODING;
        }
    }

    /**
     * 根据upload.properties文件内容构建UploadProperty对象
     * @param contextPath :
     */
    UploadProperty(String contextPath) {

        propertiesMan = new PropertiesMan(contextPath + Const.PROPERTIES_UPLOAD);

        setCacheSize();

        setFullTempPath();

        setFileSizeMax();

        setAllFileSizeMax();

        setEncoding();
    }

    /**
     * <p>从配置文件中读取单次上传文件合计大小的最大值(KB)</p>
     * 若读取失败, 则使用默认值
     */
    private void setAllFileSizeMax() {
        String allFileSizeMax = propertiesMan.getValue(Const.UPLOAD_ALLFILESIZEMAX);
        isNumeric(allFileSizeMax);
    }

    /**
     * <p>取缓冲区的大小(KB)</p>
     * 若读取失败, 则使用默认值
     */
    private void setCacheSize() {
        String cacheSizeStr = propertiesMan.getValue(Const.UPLOAD_CACHE_SIZE);
        AtomicInteger cacheSize = new AtomicInteger();
        if(isNumeric(cacheSizeStr)){
            cacheSize.set(Integer.parseUnsignedInt(cacheSizeStr.trim()));
        }else {
            cacheSize.set(Const.UPLOAD_DEF_CACHE_SIZE);
        }
    }

    /**
     * <p>设置上传单个文件大小的最大值(KB)</p>
     * 若读取失败, 则使用默认值
     */
    private void setFileSizeMax() {
        String fileSizeMax = propertiesMan.getValue(Const.UPLOAD_FILESIZEMAX);
        isNumeric(fileSizeMax);
    }


    /**
     * 根据上传目标文件夹名取上传文件夹的相对(应用的)路径
     * @param aimFolderName : 上传目标文件夹名(如:image)
     * @return 返回相对(应用的)路径(例:upload\image\)
     *  此处upload是从属性文件中读出的整个应用的上传文件夹名
     */
    public String getUploadRelativePath(String aimFolderName) {
        String upCompoundPath = getUploadRelativePath() + aimFolderName;
        return upCompoundPath + File.separator;
    }

    /**
     * 从配置文件中读取此应用的总上传目录的相对(应用的)路径,例:upload/
     * @return
     */
    private String getUploadRelativePath() {
        String upFolder = propertiesMan.getValue(Const.UPLOAD_FOLDER);
        return upFolder + File.separator;
    }

    /**
     * 根据配置文件中记录的临时文件目录名, 构建临时文件夹全路径
     * @return
     */
    private void setFullTempPath(){
    }

    private boolean isNumeric(String str){
        if(null == str){
            return false;
        }
        Pattern pattern = Pattern.compile("[0-9]*");
        return pattern.matcher(str).matches();
    }
}
