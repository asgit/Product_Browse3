package cn.wzr.util.upload;

public interface UploadStrategy {

    String getUploadRelativePath(String entityFolderName);

}
