package cn.wzr.util;

import cn.wzr.dao.impl.CommodityDao;
import cn.wzr.global.Const;

import java.io.File;
import java.util.regex.Pattern;

public class CommonTools {
    /**
     * 去掉指定字符串的开头和结尾的指定字符
     *
     * @param redanStr 原始字符串
     * @param trimStr  要删除的字符串
     * @return
     */
    public static String StringTrim(String redanStr, String trimStr) {
        // null或者空字符串的时候不处理
        if (redanStr == null || redanStr.length() == 0 || trimStr == null || trimStr.length() == 0
                || trimStr.length() > redanStr.length()) {
            return redanStr;
        }
        // 结束标志
        boolean endFlag = false;
        int point = 0;
        int move = trimStr.length();
        String clearStr = redanStr.replaceAll("^" + trimStr + "+|" + trimStr + "$", "");
        return clearStr;
    }

    /**
     * 读取数据库配置信息, 新建一个CommodityDao
     *
     * @param appPath 应用的实际路径
     * @return CommodityDao 对象
     */
    public static CommodityDao getCommodityDao(String appPath) {
        String dbPropertiesPath = appPath + File.separator
                + Const.PROPERTIES_DBCONFIG;
        DbConfig dbConfig = new DbConfig(dbPropertiesPath);
        String url = dbConfig.getUrl();
        String dbuser = dbConfig.getUserName();
        String dbpassword = dbConfig.getPassword();
        String dbDriver = dbConfig.getDriver();
        return new CommodityDao(url, dbDriver, dbuser, dbpassword);
    }

    /**
     * 取文件扩展名(包括点号)
     *
     * @param file_name
     * @returns {string}
     */
    public static String getExtension(String file_name) {
        int indexExt = file_name.lastIndexOf('.');
        String result = (0 <= indexExt) ? file_name.substring(indexExt) : "";
        return result;
    }


    /**
     * 判断文件是否存在, 可读, 大小不为0, 扩展名不为空
     * @param file
     * @return
     */
    public static boolean isReadableExtFile(File file) {
        if (null == file || !file.exists() || !file.isFile() || file.getName().isEmpty()
                || !file.canRead() || 0 == file.length()) {
            return false;
        }

        if (getExtension(file.getName()).isEmpty()) {
            return false;
        }
        return true;
    }

    /**
     * 传入一个文件夹的绝对路径字符串, 创建所有路径上不存在的文件夹
     *
     * @param folderPath
     * @return
     */
    public static boolean makePathDirs(String folderPath) {
        File file = new File(folderPath);
        if (!file.exists()) {
            return file.mkdirs();
        }
        return false;
    }

    /**
     * 判断是否为正整数
     *
     * @param str
     * @return
     */
    public static boolean isPositiveInt(String str) {
        if (null == str) {
            return false;
        }
        Pattern pattern = Pattern.compile("[0-9]*");
        return pattern.matcher(str).matches();
    }

/*
    public static Map<String, String> getMapFromParts(Collection<Part> parts) {
        if (null == parts) {
            return null;
        }
        Map<String,String> map = new HashMap<>();
            for(Part part : parts){
                if(null == part.getContentType()){
                    // 非二进制文件, 生成Map
                    map.put(part.getName(), part.)

                }
            }

    }*/
}
